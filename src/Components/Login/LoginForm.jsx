import React from 'react'
import './LoginForm.css';
import { FaUser,FaLock } from "react-icons/fa";


const LoginForm = () => {
  return (
    <div className='wrapper'>
        <form action="">
            <h1>Club Alemán</h1>
            <div className="input-box">
                <input type="text" placeholder='Usuario' required />
                <FaUser className='icon'/>
            </div>
            <div className="input-box">
                <input type="password" placeholder='Password' required />
                <FaLock className='icon'/>
            </div>

            <button type="submit">Entrar</button>
        </form>
    </div>
  )
}

export default LoginForm