import LoginForm from './Components/Login/LoginForm';

function App() {
  return (
    <div className="App">
      <header>
        <LoginForm />
      </header>
    </div>
  );
}

export default App;
